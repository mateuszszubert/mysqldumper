#!/bin/bash
dbName=$1
dbUser=$2
dbPassword=$3
isEmptyDbPassword=$([[ ! -z $dbPassword ]]; echo $?)
execDate=$(date +'%Y-%m-%d')
dumpPath="$4_$execDate.sql.gz"

if [[ ! -z $dbName  && ! -z $4 && ! -z $dbUser && !$isEmptyDbPassword ]] ; then
    echo "Dumping database into $dumpPath"
    /usr/bin/mysqldump -u $dbUser -p$dbPassword "$dbName" 2>/dev/null | /bin/gzip > "$dumpPath"
else
    echo "Parameters not provided (dbName (#1): $dbName, dumpPath (#2): $dumpPath, dbUser (#3): $dbUser, isEmptyDbPassword (#4): $isEmptyDbPassword)."
fi
